from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from shoes_rest.models import BinVo, Shoe

# Create your views here.
class BinVoEncoder(ModelEncoder):
    model = BinVo
    properties = ["closet_name", "import_href", "bin_number"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["model"]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model",
        "color",
        "picture",
        "bin",
    ]
    encoders = {
        "bin": BinVoEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            bin = BinVo.objects.get(id=content["bin"])
            content["bin"] = bin
        except BinVo.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        new_shoe = Shoe.objects.create(**content)
        return JsonResponse(
            new_shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_shoes_detail(request, pk):
    if request.method == "GET":
        shoes = Shoe.objects.get(id=pk)
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder,
        )
    elif request.method == "DELETE":
        try:
            shoes = Shoe.objects.get(id=pk)
            shoes.delete()
            return JsonResponse(
                shoes,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})