from django.urls import path
from .api_views import api_list_shoes, api_shoes_detail

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_create_shoe"),
    path("bins/<int:bin_vo_id>/shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:pk>/", api_shoes_detail, name="api_shoes_detail"),
]