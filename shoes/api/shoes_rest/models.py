from django.db import models
from django.urls import reverse

# Create your models here.


class BinVo(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return self.closet_name


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture = models.URLField(null=True)
    
    bin = models.ForeignKey(
        BinVo,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.model
    
    def get_api_url(self):
        return reverse("api_shoes_detail", kwargs={"pk": self.pk})
    
    class Meta:
        ordering = ("manufacturer", "model", "color")
    