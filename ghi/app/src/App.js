import { BrowserRouter, Routes, Route } from "react-router-dom";
import { HatComp } from "./components/hats";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ShoesList from './ShoesList'
import ShoesForm from './ShoesForm'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
        </Routes>
        <Routes>
          <Route path="/" element={<MainPage />} />
        </Routes>
        <Routes>
          <Route path="/shoes" element={<ShoesList />} />
        </Routes>
        <Routes>
          <Route path="/shoes/add" element={<ShoesForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
