import { useEffect, useState } from 'react'
import { useParams, Link } from 'react-router-dom';
import { NavLink } from 'react-router-dom';

function ShoesList() {
    const [shoes, setShoes] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/bins/shoes/');
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    const handleDelete = async (s) => {
        const url = `http://localhost:8080/bins/shoes/${s.target.id}/`

        const fetchConfig = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const response = await fetch(url, fetchConfig)
        const data = await response.json()
        setShoes(shoes.filter(shoe => String(shoe.id) !== s.target.id));
    }

    return (
        <table className='table'>
            <thead>
                <tr>
                    <th>Shoe ID</th>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Bin</th>
                    <th>delete?</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td>{shoe.id}</td>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.model}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.bin}</td>
                            <td><button onClick={handleDelete} id={shoe.id} className='btn btn-danger'>Adios!</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}


export default ShoesList;