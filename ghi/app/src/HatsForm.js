import React, { useEffect, useState } from "react";

function HatsForm() {
  const [locations, setLocations] = useState([])

  const [formData, setFormData] = useState({
    fabric: '',
    style_name: '',
    color: '',
    location: '',
  })

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (location) => {
    location.preventDefault();

    const url = 'http://localhost:8090/locations/hats/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      }
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        fabric: '',
        style_name: '',
        color: '',
        location: '',
      });
    }
  }

  const handleFormChange = (l) => {
    const value = l.target.value;
    const inputModel = l.target.model;

    setFormData({
      ...formData,
      [inputModel]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Hat</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
              <label htmlFor="Fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.style_name} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
              <label htmlFor="style_name">Style Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.location} required name="location" id="location" className="form-select">
                <option value="">Choose a Location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.closet_name} Shelf (location.shelf_number)
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );

}

export default HatsForm;
