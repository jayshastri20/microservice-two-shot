import { useEffect, useState } from 'react'
import { useParams, Link } from 'react-router-dom';
import { NavLink } from 'react-router-dom';

function HatsList() {
    const [hats, setHats] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/locations/hats/');
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    const handleDelete = async (h) => {
        const url = `http://localhost:8090/locations/hats/${h.target.id}/`

        const fetchConfig = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const response = await fetch(url, fetchConfig)
        const data = await response.json()
        setHats(hats.filter(hat => String(hat.id) !== h.target.id));
    }

    return (
        <table className='table'>
            <thead>
                <tr>
                    <th>Hat ID</th>
                    <th>Fabric</th>
                    <th>Style Name</th>
                    <th>Color</th>
                    <th>Location</th>
                    <th>delete?</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{hat.id}</td>
                            <td>{hat.fabric}</td>
                            <td>{hat.style_name}</td>
                            <td>{hat.color}</td>
                            <td>{hat.location}</td>
                            <td><button onClick={handleDelete} id={hat.id} className='btn btn-danger'>Adios!</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}


export default HatsList;