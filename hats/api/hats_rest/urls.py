from django.urls import path
from .views import api_list_hats, api_hats_detail

urlpatterns = [
    path(
        "locations/<int:location_vo_id>/hats/",
        api_list_hats,
        name="list_locations",
    ),
    path("hats/", api_list_hats, name="create_hats"),
    path("hats/<int:pk>/", api_hats_detail, name="hat_details"),
    path("hats/<int:location_vo_id>/", api_list_hats, name="list_hats"),
]
